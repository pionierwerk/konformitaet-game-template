------------------------------------------------------------------------------------
-- kfcMenu
-- custom Menu functions
--
-- @author þorN 
-- @license zlib
-- @copyright þorN  for pionierwerk.eu 2018
------------------------------------------------------------------------------------

-- *********************************************************************************
-- Control functions
------------------------------------------------------------------------------------
--- Switch from time to lifes game type.
function switchToLifesGameType()
    if kf.lifes_or_time_mode == kf.MODE_TIME then                                   -- test if mode = tine
        kf.vars["time_start"] = kf.states["menu"].controls[MENU_TIME_CONTROL].model["zero_value"]
        kf.vars["time_start"] = 0

        kf.lifes_or_time_mode = kf.MODE_LIFES
        kf.hs_lifes_or_time_mode = kf.MODE_LIFES
    end
    
    kf:calculateGameType({variable_to_set = "actual_game_type", high_score_type_state = "menu"}) -- calculate actual game type. This creates the table for the actual game type
end

------------------------------------------------------------------------------------
--- Switch from lifes to to time game type
function switchToTimeGameType()
    if kf.lifes_or_time_mode == kf.MODE_LIFES then                                  -- test if mode = lifes
        kf.vars["lifes"] = kf.states["menu"].controls[MENU_LIFES_CONTROL].model["zero_value"]
        kf.lifes_or_time_mode = kf.MODE_TIME
        kf.hs_lifes_or_time_mode = kf.MODE_TIME
    end
    
    kf:calculateGameType()                                                          -- calculate actual game type. This creates the table for the actual game type
end

------------------------------------------------------------------------------------
--- Switch from time to lifes game type in the high score viewer
function switchToLifesGameTypeHS()
    if kf.hs_lifes_or_time_mode == kf.MODE_TIME then                                -- test if mode = tine
        kf.vars["hs_time_start"] = kf.states["high_score_viewer"].controls[MENU_TIME_CONTROL].model["zero_value"]
        kf.hs_lifes_or_time_mode = kf.MODE_LIFES
    end
    
    kf:calculateHsGameType()                                                        -- calculate actual high score viewer game type. This creates the table for the actual game type
end

------------------------------------------------------------------------------------
--- Switch from lifes to time game type in the high score viewer
function switchToTimeGameTypeHS()
    if kf.hs_lifes_or_time_mode == kf.MODE_LIFES then                               -- test if mode = lifes
        kf.vars["hs_lifes"] = kf.states["high_score_viewer"].controls[MENU_LIFES_CONTROL].model["zero_value"]

        kf.hs_lifes_or_time_mode = kf.MODE_TIME
    end
    
    kf:calculateHsGameType()                                                        -- calculate actual high score viewer game type. This creates the table for the actual game type
end

------------------------------------------------------------------------------------
--- Switch from lifes to time game type when the number of players is highter than 1
function switchToTimeGameTypeOnMultiPlayerGame()
    if kf.vars["kf_number_of_players"] == kf.MIN_NUMBER_OF_PLAYER_IN_A_MULTI_PLAYER_GAME then -- test if the border from single o multi player game was crossed
        if kf.lifes_or_time_mode == kf.MODE_LIFES then                              -- test if mode = lifes
            kf.vars["lifes"] = kf.states["menu"].controls[MENU_LIFES_CONTROL].model["zero_value"]
            kf.lifes_or_time_mode = kf.MODE_TIME
            kf.hs_lifes_or_time_mode = kf.MODE_TIME
            kf.vars["time_start"] = MINIMUM_TIME
        end
        
        kf:calculateGameType()                                                      -- calculate actual game type. This creates the table for the actual game type
    end
end

-- *********************************************************************************
-- display rules
------------------------------------------------------------------------------------
--- display lifes only if it is a single player game
function display_rule_lifes()
    return kf.vars["kf_number_of_players"] == kf.NUMBER_OF_PLAYER_IN_A_SINGLE_PLAYER_GAME -- test if number of players match the definition for single player games
end

------------------------------------------------------------------------------------
--- display a high score viewer only in a single player game
function display_rule_high_scores()
    return kf.vars["kf_number_of_players"] == kf.NUMBER_OF_PLAYER_IN_A_SINGLE_PLAYER_GAME -- test if number of players match the definition for single player games
end

-- *********************************************************************************
-- overloaded functions
------------------------------------------------------------------------------------
--- set maximum number of players to the number of connected joysticks
-- @param                   model                   the control model
function overload_players_max(model)
    model["max"] = #kf.pads                                                         -- set the maximum number of playerss to the number of connected joysticks
end
