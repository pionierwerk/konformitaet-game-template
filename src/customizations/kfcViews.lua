------------------------------------------------------------------------------------
-- kfcViews.lua
-- Lib for custom views
--
-- @author þorN 
-- @license zlib
-- @copyright þorN  for pionierwerk.eu 2018
------------------------------------------------------------------------------------

kfcViews = {}

--- render the logo view.
-- ---------------------------------------------------------------------------------
function kfcViews:logo()
    love.graphics.setColor(kf.COLOR_BLACK)
    love.graphics.rectangle("fill",0,0,kf.vars["kf_width"],kf.vars["kf_height"])    -- fill screen with a black backround
    love.graphics.setColor(kf.COLOR_WHITE) 
    love.graphics.draw(assets.img.pionierwerk,LOGO_X,LOGO_Y)                        -- draw logo
end

--- render the story view.
-- ---------------------------------------------------------------------------------
function kfcViews:story()
    kfcViews:template("STORY")                                                      -- render menu template

    kfUtils:displayTextAndMenu("story",STORY_TEXTS,MENU_START_HEIGHT,MENU_ROW_HEIGHT) -- render text and menu
end

--- render the menu view.
-- ---------------------------------------------------------------------------------
function kfcViews:menu()
    kfcViews:template("MAIN MENU")                                                  -- render menu template

    kfUtils:displayMenu("menu",MENU_START_HEIGHT)                                   -- render menu

    -- Version
    love.graphics.setColor(kf.COLOR_WHITE)                                          -- render version
    love.graphics.printf("Version " .. kf.vars["kf_version_main"] .. "." .. kf.vars["kf_version_feature"] .. "." .. kf.vars["kf_version_minor"],CENTERED,MENU_START_HEIGHT + VERSION_NUMBER_ROW * MENU_ROW_HEIGHT,kf.vars["kf_width"],"center",ORIENTATION,TEXT_X_SCALE,TEXT_Y_SCALE,ORIGIN_X_OFFSET,ORIGIN_Y_OFFSET,SHEARING_X_FACTOR,SHEARING_Y_FACTOR)                    
end

--- render the ok cancel dialog view.
-- ---------------------------------------------------------------------------------
function kfcViews:okcanceldialog()
    -- render frame
    local _y_offset = kfUtils:displayFrame(kf.model["x"],kf.model["y"],kf.model["width"],kf.model["height"],kf.model["texts"])

    kfUtils:displayMenu("okcanceldialog",_y_offset)                                 -- render menu
end

--- render the high scores viewer view
-- ---------------------------------------------------------------------------------
function kfcViews:high_score_viewer()
    kfcViews:template("HIGH SCORES")                                                -- render menu template

    -- highscore menu
    kfUtils:displayMenu("high_score_viewer",MENU_START_HEIGHT)                      -- render menu

    -- game type
    love.graphics.setColor(kf.COLOR_WHITE)                                          -- print high score viewer game type
    love.graphics.printf(kfUtils:getActualGameTypeDescription("high_score_viewer"),CENTERED,HIGHSCORE_VIEWER_GAME_TYPE_Y,kf.vars["kf_width"],"center",ORIENTATION,TEXT_X_SCALE,TEXT_Y_SCALE,ORIGIN_X_OFFSET,ORIGIN_Y_OFFSET,SHEARING_X_FACTOR,SHEARING_Y_FACTOR)  


    -- highscores
    for i=1,HIGHSCORE_SCORES_TO_SHOW do                                             -- loop over high scores
        local _score = kfUtils:normalizeStringLength(tostring(i),HIGHSCORE_PLACE_STRING_LENGTH,"0",0) -- calculate place string
        local _name = kf.high_score_data_base.tables[kf.hs_actual_game_type][i].name-- get name
        -- get points
        local _points = kfUtils:normalizeStringLength(tostring(kf.high_score_data_base.tables[kf.hs_actual_game_type][i].points),HIGHSCORE_POINT_STRING_LENGTH,"0",0)
        local _filler = "."                                                         -- set the ... to complete the line

        for f = 1,(kf.vars["kf_max_chars_in_name"] - #_name) * FILLER_FACTOR do     -- add dots to the name
            _filler = _filler .. "."
        end
        
        -- print high scores
        love.graphics.printf(_score,HIGHSCORE_SCORE_X,HIGHSCORE_VIEWER_Y_OFFSET + i * MENU_ROW_HEIGHT,kf.vars["kf_width"],nil,ORIENTATION,TEXT_X_SCALE,TEXT_Y_SCALE,ORIGIN_X_OFFSET,ORIGIN_Y_OFFSET,SHEARING_X_FACTOR,SHEARING_Y_FACTOR)  
        love.graphics.printf(_name,HIGHSCORE_NAME_X,HIGHSCORE_VIEWER_Y_OFFSET + i * MENU_ROW_HEIGHT,kf.vars["kf_width"],nil,ORIENTATION,TEXT_X_SCALE,TEXT_Y_SCALE,ORIGIN_X_OFFSET,ORIGIN_Y_OFFSET,SHEARING_X_FACTOR,SHEARING_Y_FACTOR)  

        love.graphics.printf(_filler,HIGHSCORE_FILLER_X - #_filler * FILLER_X_LENGTH ,HIGHSCORE_VIEWER_Y_OFFSET + i * MENU_ROW_HEIGHT,kf.vars["kf_width"],nil,ORIENTATION,TEXT_X_SCALE,TEXT_Y_SCALE,ORIGIN_X_OFFSET,ORIGIN_Y_OFFSET,SHEARING_X_FACTOR,SHEARING_Y_FACTOR)  

        love.graphics.printf(_points,HIGHSCORE_POINTS_X,HIGHSCORE_VIEWER_Y_OFFSET + i * MENU_ROW_HEIGHT,kf.vars["kf_width"],nil,ORIENTATION,TEXT_X_SCALE,TEXT_Y_SCALE,ORIGIN_X_OFFSET,ORIGIN_Y_OFFSET,SHEARING_X_FACTOR,SHEARING_Y_FACTOR)  
    end
end

--- render the credits view.
-- ---------------------------------------------------------------------------------
function kfcViews:credits()
    kfcViews:template("CREDITS")                                                    -- render menu template

    kfUtils:displayTextAndMenu("credits",CREDIT_TEXTS,MENU_START_HEIGHT,MENU_ROW_HEIGHT) -- render text and menu
end

--- render the how to play view.
-- ---------------------------------------------------------------------------------
function kfcViews:howtoplay()
    kfcViews:template("HOW TO PLAY")                                                -- render menu template

    kfUtils:displayTextAndMenu("howtoplay",HOW_TO_PLAY_TEXTS,MENU_START_HEIGHT,MENU_ROW_HEIGHT) -- render text and menu
end

--- render the enter name view.
-- ---------------------------------------------------------------------------------
function kfcViews:name()
    kfcViews:template("ENTER YOUR NAME FOR THE HIGH SCORE TABLE")                   -- render menu template

    local rows = ENTER_NAME_ROWS                                                    -- the number of rows to render
    local columns = ENTER_NAME_COLUMNS                                              -- the number of columns to render

    local chars={                                                                   -- the chars controls
        "a","b","c","d","e","f","g","h","i","j","k","l",
        "m","n","o","p","q","r","s","t","u","v","w","x",
        "y","z","n0","n1","n2","n3","n4","n5","n6","n7","n8","n9",
        "sminus","spoint","sdoublepoint","shash","sbracketleft","sbracketright","splus","sstar","sexclamationmark","squestionmark","sand","sequal",
        "space","clear","back","ok"
    }

    for row=1,rows do                                                               -- loop over the rows
        for column=1,columns do                                                     -- loop over the columns
            local i=(row-1)*columns+column                                          -- the number of the actual char control 

            love.graphics.setColor(kf.COLOR_WHITE)                                  -- set the color to not highlighted
            if chars[i] == kf.model.actual_control_name then                        -- test if the actual char is the one that is selected
                love.graphics.setColor(kf.COLOR_SELECTED)                           -- yes: set the color to highlite
            end

            local _x = (kf.vars["kf_width"] - (columns * (CHARS_COLUMN_WIDTH+1)) - CHARS_X_OFFSET) / kf.CENTER -- calulate x position

            if i <= #chars then                                                     -- test if the number of the char control is lower than the total number of char controls
                local _control = kf.states["name"].controls[chars[i]]               -- get the actual control
                local _name = _control.model["display_name"]                        -- get the display name of the control

                if chars[i] == "space" then                                         -- print space
                    love.graphics.printf(_name,_x + SPACE_X_OFFSET ,MENU_START_HEIGHT + (row * MENU_ROW_HEIGHT),kf.vars["kf_width"],nil,ORIENTATION,TEXT_X_SCALE,TEXT_Y_SCALE,ORIGIN_X_OFFSET,ORIGIN_Y_OFFSET,SHEARING_X_FACTOR,SHEARING_Y_FACTOR)                    
                elseif chars[i] == "clear" then                                     -- print clear
                    love.graphics.printf(_name,_x + CLEAR_X_OFFSET,MENU_START_HEIGHT + (row * MENU_ROW_HEIGHT),kf.vars["kf_width"],nil,ORIENTATION,TEXT_X_SCALE,TEXT_Y_SCALE,ORIGIN_X_OFFSET,ORIGIN_Y_OFFSET,SHEARING_X_FACTOR,SHEARING_Y_FACTOR)                    
                elseif chars[i] == "back" then                                      -- print back
                    love.graphics.printf(_name,_x + BACK_X_OFFSET,MENU_START_HEIGHT + (row * MENU_ROW_HEIGHT),kf.vars["kf_width"],nil,ORIENTATION,TEXT_X_SCALE,TEXT_Y_SCALE,ORIGIN_X_OFFSET,ORIGIN_Y_OFFSET,SHEARING_X_FACTOR,SHEARING_Y_FACTOR)                    
                elseif chars[i] == "ok" then                                        -- print ok
                    love.graphics.printf(_name,_x + OK_X_OFFSET,MENU_START_HEIGHT + (row * MENU_ROW_HEIGHT),kf.vars["kf_width"],nil,ORIENTATION,TEXT_X_SCALE,TEXT_Y_SCALE,ORIGIN_X_OFFSET,ORIGIN_Y_OFFSET,SHEARING_X_FACTOR,SHEARING_Y_FACTOR)                    
                else                                                                -- print other chars
                    love.graphics.printf(_name,_x + column * CHARS_COLUMN_WIDTH,MENU_START_HEIGHT + (row * MENU_ROW_HEIGHT),kf.vars["kf_width"],nil,ORIENTATION,TEXT_X_SCALE,TEXT_Y_SCALE,ORIGIN_X_OFFSET,ORIGIN_Y_OFFSET,SHEARING_X_FACTOR,SHEARING_Y_FACTOR)                    
                end
            end
        end
    end
 
    -- print the players name
    love.graphics.printf("[" .. kf.high_score_data_base.player_name .."]",CENTERED,MENU_START_HEIGHT + (TEXT_Y_OFFSET * MENU_ROW_HEIGHT),kf.vars["kf_width"],"center",ORIENTATION,TEXT_X_SCALE,TEXT_Y_SCALE,ORIGIN_X_OFFSET,ORIGIN_Y_OFFSET,SHEARING_X_FACTOR,SHEARING_Y_FACTOR)
end

--- render the highscore table view.
-- ---------------------------------------------------------------------------------
function kfcViews:high_score_table()
    kfcViews:template("HIGH SCORE TABLE")

    -- game type
    love.graphics.printf(kfUtils:getActualGameTypeDescription("menu"),CENTERED,MENU_START_HEIGHT + (HIGHSCORE_TABLE_GAME_TYPE_ROW * MENU_ROW_HEIGHT),kf.vars["kf_width"],"center",ORIENTATION,TEXT_X_SCALE,TEXT_Y_SCALE,ORIGIN_X_OFFSET,ORIGIN_Y_OFFSET,SHEARING_X_FACTOR,SHEARING_Y_FACTOR)  

    -- scores
    local _number_of_entries = HIGHSCORE_SCORES_TO_SHOW                             -- initialize the value with the amount of ros in single player games

    if kf.vars["kf_number_of_players"] > kf.NUMBER_OF_PLAYER_IN_A_SINGLE_PLAYER_GAME then -- test if it is a multi player game
        _number_of_entries = kf.vars["kf_number_of_players"]
    end

    for i=1,_number_of_entries do
        local _score = kfUtils:normalizeStringLength(tostring(i),HIGHSCORE_PLACE_STRING_LENGTH,"0",0)
        local _name = kf.high_score_data_base.tables[kf.actual_game_type][i].name
        local _points = kfUtils:normalizeStringLength(tostring(kf.high_score_data_base.tables[kf.actual_game_type][i].points),HIGHSCORE_POINT_STRING_LENGTH,"0",0)
        local _filler = "."

        for f = 1,(kf.vars["kf_max_chars_in_name"] - #_name) * FILLER_FACTOR do     -- add dots to the name
            _filler = _filler .. "."
        end
        
        love.graphics.printf(_score,HIGHSCORE_SCORE_X,MENU_START_HEIGHT + ((HIGHSCORE_TABLE_GAME_TYPE_ROW + i) * MENU_ROW_HEIGHT) + HIGHSCORE_Y_OFFSET,kf.vars["kf_width"],nil,ORIENTATION,TEXT_X_SCALE,TEXT_Y_SCALE,ORIGIN_X_OFFSET,ORIGIN_Y_OFFSET,SHEARING_X_FACTOR,SHEARING_Y_FACTOR)  
        love.graphics.printf(_name,HIGHSCORE_NAME_X,MENU_START_HEIGHT + ((HIGHSCORE_TABLE_GAME_TYPE_ROW + i) * MENU_ROW_HEIGHT) + HIGHSCORE_Y_OFFSET,kf.vars["kf_width"],nil,ORIENTATION,TEXT_X_SCALE,TEXT_Y_SCALE,ORIGIN_X_OFFSET,ORIGIN_Y_OFFSET,SHEARING_X_FACTOR,SHEARING_Y_FACTOR)  
        love.graphics.printf(_filler,HIGHSCORE_FILLER_X - #_filler * FILLER_X_LENGTH ,MENU_START_HEIGHT + ((HIGHSCORE_TABLE_GAME_TYPE_ROW + i) * MENU_ROW_HEIGHT) + HIGHSCORE_Y_OFFSET,kf.vars["kf_width"],nil,ORIENTATION,TEXT_X_SCALE,TEXT_Y_SCALE,ORIGIN_X_OFFSET,ORIGIN_Y_OFFSET,SHEARING_X_FACTOR,SHEARING_Y_FACTOR)  
        love.graphics.printf(_points,HIGHSCORE_POINTS_X,MENU_START_HEIGHT + ((HIGHSCORE_TABLE_GAME_TYPE_ROW + i) * MENU_ROW_HEIGHT) + HIGHSCORE_Y_OFFSET,kf.vars["kf_width"],nil,ORIENTATION,TEXT_X_SCALE,TEXT_Y_SCALE,ORIGIN_X_OFFSET,ORIGIN_Y_OFFSET,SHEARING_X_FACTOR,SHEARING_Y_FACTOR)  
    end

    -- menu
    kfUtils:displayMenu("high_score_table",MENU_START_HEIGHT + ((_number_of_entries + HIGHSCORE_TABLE_EXTRA_LINES) * MENU_ROW_HEIGHT)) -- add the menu (ok button)
end

--- render the game start view.
-- ---------------------------------------------------------------------------------
function kfcViews:game_starts()
    kfUtils:overlayImageAndCounter(                                                 -- render "get ready " and a counter      
        {kfcViews.game},assets.img.getready,
        {
            assets.img.drei,
            assets.img.zwei,
            assets.img.eins
        },
        OVERLAY_TIMER_START,OVERLAY_DURATION,OVERLAY_IMAGE_Y,OVERLAY_NUMBER_Y
    )
end

--- render the game view.
-- ---------------------------------------------------------------------------------
function kfcViews:game()
    -- render scores & time or lifes

    local _score_string = ""
    local _sx = GAME_SCORE_X_MULTI
    local _tx = GAME_TIME_X_MULTI

    for key,player in pairs(kf.vars["kf_players"]) do
        if kf.vars["kf_number_of_players"] == kf.NUMBER_OF_PLAYER_IN_A_SINGLE_PLAYER_GAME then
            _sx = GAME_SCORE_X
            _tx = GAME_TIME_X
            _score_string = "Score:" .. kfUtils:normalizeStringLength(tostring(player.score),HIGHSCORE_POINT_STRING_LENGTH,"0",0)
        else
            _score_string = _score_string .. "PL" .. key .. ":" .. kfUtils:normalizeStringLength(tostring(player.score),HIGHSCORE_POINT_STRING_LENGTH,"0",0) .. " "
        end
    end

    love.graphics.setColor(kf.COLOR_WHITE) 
    love.graphics.printf(_score_string,_sx,TEXT_Y_OFFSET,kf.vars["kf_width"],nil,ORIENTATION,TEXT_X_SCALE,TEXT_Y_SCALE,ORIGIN_X_OFFSET,ORIGIN_Y_OFFSET,SHEARING_X_FACTOR,SHEARING_Y_FACTOR)
    if kf.lifes_or_time_mode == kf.MODE_LIFES then                                  -- decide if time or lifes mode
        love.graphics.printf("Lives:" .. kf.vars["kf_players"][kf.PAD_MASTER].lifes,GAME_LIFES_X,TEXT_Y_OFFSET,kf.vars["kf_width"],nil,ORIENTATION,TEXT_X_SCALE,TEXT_Y_SCALE,ORIGIN_X_OFFSET,ORIGIN_Y_OFFSET,SHEARING_X_FACTOR,SHEARING_Y_FACTOR)
    else
        love.graphics.printf("Time:" .. kfUtils:normalizeStringLength(tostring(kf.time),HIGHSCORE_TIME_STRING_LENGTH,"0",0),_tx,TEXT_Y_OFFSET,kf.vars["kf_width"],nil,ORIENTATION,TEXT_X_SCALE,TEXT_Y_SCALE,ORIGIN_X_OFFSET,ORIGIN_Y_OFFSET,SHEARING_X_FACTOR,SHEARING_Y_FACTOR)
    end

    -- sprites
    -- -----------------------------------------------------------------------------
    for layer =1,#kf.sprites do                                                     -- loop over layers
        for key,sprite in pairs(kf.sprites[layer]) do                               -- loop over sprites
            if sprite.isActive then                                                 -- test if sprite is active
                love.graphics.draw(sprite.sheet,sprite.image,sprite.x,sprite.y)     -- yes: draw sprite
            end
        end
    end
end

--- render the game over view.
-- ---------------------------------------------------------------------------------
function kfcViews:game_over()
    kfUtils:overlayImageAndCounter(                                                 -- render "game over" and a counter  
        {
            kfcViews.game
        },
        assets.img.gameover,
        {
            assets.img.drei,
            assets.img.zwei,
            assets.img.eins
        },
        OVERLAY_TIMER_START,OVERLAY_DURATION,OVERLAY_IMAGE_Y,OVERLAY_NUMBER_Y
    )
end

-- helper functions
-- ================================================================================================================================
--- the menu screen template, used in most menu views.
-- @param headline the headline to render in a view
-- ---------------------------------------------------------------------------------
function kfcViews:template(headline)
    love.graphics.setColor(kf.COLOR_WHITE)
    
    for letter =0,SCROLL_TEXT_NUMBER_OF_CHARS do                                    -- loop of chars in scroll text and print them
       love.graphics.printf(string.sub(SCROLL_TEXT, kf.vars["scroll_text_offset"] + letter+1, kf.vars["scroll_text_offset"] + letter+1), kf.vars["scroll_text_x"] + (letter * SCROLL_TEXT_CHAR_WIDTH),SCROLL_TEXT_Y,kf.vars["kf_width"],nil,ORIENTATION,TEXT_X_SCALE,TEXT_Y_SCALE,ORIGIN_X_OFFSET,ORIGIN_Y_OFFSET,SHEARING_X_FACTOR,SHEARING_Y_FACTOR)
    end

    -- pionierwerk logo
    love.graphics.draw(assets.img.pionierwerk_400x40px,(kf.vars["kf_width"]-assets.img.pionierwerk_400x40px:getWidth()) / KF_CENTER,kf.vars["kf_height"] - PW_LOGO_HEIGHT)
    
    kfUtils:scroll_text(SCROLL_TEXT)                                                -- render a scroller
end