kfcTransitions = {}

------------------------------------------------------------------------------------
--- Transition between init and logo, init application
function kfcTransitions:init2logo()
    love.graphics.setBackgroundColor(kf.COLOR_BLACK)                                -- set background to black
    love.graphics.setFont(kf.vars["game_font"])                                     -- set game font        
    assets.mp3.template_track:setVolume(0.5)
    assets.mp3.template_track:setLooping(true) 
    assets.mp3.template_track:play()
end

------------------------------------------------------------------------------------
--- Transition between menu and game_starts, initialize the game round.
function kfcTransitions:menu2game_starts()
    -- initialize start points
    kf.vars["start_points"] = Startpoints:new()                                     -- create a new set of start points

    -- initialize sprites
    kf.sprites = {}                                                               -- clear sprites   
    -- initialize players
    kf.vars["kf_players"] = {}                                                      -- clear players

    for pl_nr=1,kf.vars["kf_number_of_players"] do                                  -- loop over the numbe of players
        local _player = Player:new(5,0,(pl_nr-1)*PLAYER_QUAD_HEIGHT,true)           -- create player  
        _player.lifes = kf.vars["lifes"]                                            -- set number of lifes 
        kf.vars["kf_players"][pl_nr] = _player                                      -- put new player into players
    end

    -- initialize creatures
    kf.vars["kf_actors"] = {}                                                      -- clear the creatures

    for i=1,ROW_AMOUNT do                                                           -- loop over rows
        -- create creature 
        kf.vars["kf_actors"][#kf.vars["kf_actors"]+1] = Creature:new(i,ROW_UPPER_BORDER + (i * ROW_HEIGHT),maps_creature_directions[kf.vars["map"]][#kf.vars["kf_actors"]+1],GAME_BORDERS,true)
    end

    -- timer
    kf:initTimer(kf.vars["time_start"],KF_TIME_DELAY_START)                         -- initialize timer

    -- multi player high scores
    if kf.vars["kf_number_of_players"] > kf.NUMBER_OF_PLAYER_IN_A_SINGLE_PLAYER_GAME then -- test if it is a multi player game
        kf.high_score_data_base:NegateScores(kf.actual_game_type)                       -- set all scores to a negativ score. So all players will have a hight scrore and enters the high socre table
    end 
end

------------------------------------------------------------------------------------
--- Transition from game_starts to game, play the game starts sound.
function kfcTransitions:game_starts2game()
    assets.sounds.start_jetztgehtslos:play()                                        -- play game starts sound
end

------------------------------------------------------------------------------------
-- Transition from name to high score table, add the score into the high score table
function kfcTransitions:name2high_score_table()
    if kf.vars["kf_number_of_players"] == kf.NUMBER_OF_PLAYER_IN_A_SINGLE_PLAYER_GAME then  -- test if it is a single player game
        print("nein")
        kf.high_score_data_base:addScore(kf.high_score_data_base.player_name, kf.vars["kf_players"][kf.NUMBER_OF_PLAYER_IN_A_SINGLE_PLAYER_GAME].score) -- add the score into the high score table
    end
end

------------------------------------------------------------------------------------
--- Transition from menu to high scores viewer, init viewer parameter with game parameters.
function kfcTransitions:menu2high_score_viewer()
    kf.vars["hs_map"] = kf.vars["map"]                                              -- map map parameter
    kf.vars["hs_forward_speed"] = kf.vars["forward_speed"]                          -- map forward speed parameter
    kf.vars["hs_backward_speed"] = kf.vars["backward_speed"]                        -- map backward speed parameter
    kf.vars["hs_time_start"] = kf.vars["time_start"]                                -- map time starts parameter
    kf.vars["hs_lifes"] = kf.vars["lifes"]                                          -- map lifes parameter

    kf.hs_lifes_or_time_mode = kf.lifes_or_time_mode                                -- map game mode parameter

    kf:calculateHsGameType()                                                        -- calculate actual high score viewer game type. This creates the table for the actual game type
end