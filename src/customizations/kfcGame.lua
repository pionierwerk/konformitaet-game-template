-----------------------------------------------------------------------------------------------------------------------------------
-- KFCGame.lua
-- the game state and its transitions
--
-- @author þorN 
-- @license zlib
-- @copyright þorN  for pionierwerk.eu 2018
-----------------------------------------------------------------------------------------------------------------------------------

-- Game state
-- ================================================================================================================================
--- member variables.
-- @table members
KFCGame = setmetatable({},{__index = KFController})
KFCGame.__index = KFCGame

-------------------------------------------------------------------------------------
--- constructor.
function KFCGame:new(model)
    local self = setmetatable(KFController.new(model), KFCGame)

    self.model = model

    return self
end

------------------------------------------------------------------------------------
--- The game state controller.
function KFCGame:main()
    local _isGameOver = false

    -- test if the game is over
    -- ****************************************************************************************************************************
    if kf.lifes_or_time_mode == kf.MODE_LIFES then                                  -- test if game is in lifes mode
        -- liffes mode
        -- -------------------------------------------------------------------------
        local _alive_counter = kf.vars["kf_number_of_players"]                      -- set alive counter to the number of players

        for key,player in pairs(kf.vars["kf_players"]) do                           -- yes: loop over players
            if  player.lifes == PLAYER_NO_LIFES_LEFT then                           -- test if player is game over
                _alive_counter = _alive_counter - 1                                 -- yes: decrease alive counter
            end
        end

        if _alive_counter == ZERO_NO_PLAYER_ALIVE then                              -- test if alive counter is zero
            _isGameOver = true                                                      -- yes: game is over
        end
    else
        -- time mode
        -- -------------------------------------------------------------------------
        if kf.lifes_or_time_mode == kf.MODE_TIME then                               -- test if the game is in time mode
            kf:handleTime(kf.vars["kf_time_change"])                                -- handle time decreasing

            if kf.time <= ZERO_TIME_GAME_OVER then                                  -- test if the time is up
                _isGameOver = true                                                  -- yes: game is over
            end
        end
    end    

    if _isGameOver then                                                             -- test if game is over
        assets.sounds.gameover_daswarsdann:play()                                   -- play this is the end sound
        kf:endGame()                                                                -- end the game because the time is up
    end

    -- Player logic
    -- ****************************************************************************************************************************
    for pl_nr,player in pairs(kf.vars["kf_players"]) do                             -- loop over player
        -- test if dying and moving animation is active
        -- -------------------------------------------------------------------------
        if player.isDying then                                                      -- test if player is dying
            player:animated_dying()                                                 -- yes: animate dying
        else
            if player.isMoving then                                                 -- test if player moving
                player:move()                                                       -- yes: animate moving
            else
                local _row = math.floor((player.sprites["player"].y-ROW_UPPER_BORDER)/ROW_HEIGHT)+1   -- the actual row of the player. Used to determine if scoring points are to earn

                -- Input & player Movement
                -- -----------------------------------------------------------------
                kf:setDirection()                                                   -- get input events

                if bit32.band(kf.pad_bitmask[pl_nr], kf.PB_DPAD_UP) > 0 then        -- test if the event up was triggered
                    player:start_move(kf.vars["backward_speed"] * kf.NEGATION)      -- yes: start moving backward animation  
                    player.isWaitingAfterReset = false                              -- set flag to wait for not going down
                elseif bit32.band(kf.pad_bitmask[pl_nr], kf.PB_DPAD_DOWN) > 0 then  -- test if the event down was triggered
                    if not player.isWaitingAfterReset then                          -- yes: test if flag for not going down is not set
                        player:start_move(kf.vars["forward_speed"])                 -- yes: start moving forward animation 
                    end
                else
                    player.isWaitingAfterReset = false                              -- set flag to wait for not going down
                end

                -- Did player reaching the shore
                -- -----------------------------------------------------------------
                if player.sprites["player"].y >= PLAYER_BORDER_BOTTOM then          -- test if player reached the shore
                    player.score = player.score + 1                                 -- increase score
                    kf.vars["snds_win"][kfUtils:rnd_or_player_number(player, #kf.vars["snds_win"])]:play() -- yes: play random winning sound
                    player:reset(true)                                              -- reset player position to stay on top again
                end
            end
        end
    end

    -- Creature logic
    -- ************************************************************************************************************************
    -- Creatures movement
    -- -----------------------------------------------------------------------------
    for i = 1, #kf.vars["kf_actors"] do                                             -- loop over all actors
        if kf.vars["kf_actors"][i].isActive then                                    -- test if actor is active
            kf.vars["kf_actors"][i]:move()                                          -- yes: move actor
        end
    end

    -- collision detection
    -- ************************************************************************************************************************
    -- player - creature
    -- -----------------------------------------------------------------------------
    for player_key,player in pairs(kf.vars["kf_players"]) do                        -- loop over players
        if not player.isDying and player.isActive then                              -- test if player is not dying nor dead
            for creature_key,creature in pairs(kf.vars["kf_actors"]) do             -- no: loop over creatures
                if kfUtils:detectCollision(player.sprites["player"],creature.sprites["creature"]) then -- test collision of actual player with actual creature                   
                    player:start_dying(kfUtils:rnd_or_player_number(player,#kf.vars["snds_die"])) -- yes: start dying
                end
            end
        end
    end
end