------------------------------------------------------------------------------------
-- kfcConstants
-- custom constants
--
-- @author þorN 
-- @license zlib
-- @copyright þorN  for pionierwerk.eu 2018
------------------------------------------------------------------------------------

cConstants = {}

------------------------------------------------------------------------------------
--- initilize the custom constants
function cConstants:init()
    -- View
    -- ============================================================================================================================
    -- global
    -- ****************************************************************************************************************************
    -- text
    -- -----------------------------------------------------------------------------
    TEXT_X_SCALE = 1                                                                -- x scaling
    TEXT_Y_SCALE = 1                                                                -- y scaling
    ORIGIN_X_OFFSET = 0                                                             -- origin x offset
    ORIGIN_Y_OFFSET = 0                                                             -- origin y offset
    SHEARING_X_FACTOR = 0                                                           -- shearing x factor
    SHEARING_Y_FACTOR = 0                                                           -- shearing y factor
    ORIANTATION = 0                                                                 -- orientation
    CENTERED = 0                                                                    -- centered
    TEXT_Y_OFFSET = 7                                                              -- y position offset of texts from a row start

    -- modal dialoges
    MODAL_DIALOG_X = 210                                                            -- render the modeal dialoges a little bit more to the right

    -- rows. Rows could be earth or water (4 different deeps)
    -- -----------------------------------------------------------------------------
    ROW_HEIGHT = 30                                                                 -- row height
    ROW_AMOUNT = 4                                                                  -- number of rows
    ROW_UPPER_BORDER = 30                                                           -- row upper y position
    VERSION_NUMBER_ROW = 14                                                         -- the version is rendered in this row

    -- states
    -- ****************************************************************************************************************************
    -- Logo
    -- -----------------------------------------------------------------------------
    LOGO_X = 70                                                                     -- Logo x position
    LOGO_Y = 240                                                                    -- Logo y position

    -- Menu
    -- -----------------------------------------------------------------------------
    MENU_ROW_HEIGHT = 16                                                            -- row height
    MENU_START_HEIGHT = 144                                                         -- menu y position start height
    MENU_OPTIONS_START = 292                                                        -- menu options x start position
    MENU_OPTIONS_DOUBLEPOINT = 440                                                  -- menu : x start position
    MENU_OPTIONS_VALUE = MENU_OPTIONS_DOUBLEPOINT + 10                              -- menu value x start position
    MENU_MIDDLE_START = 122                                                         -- menu post logo section y position start

    MENU_TIME_CONTROL = 4                                                           -- number of the time control
    MENU_LIFES_CONTROL = 5                                                          -- number of the lifes control

    SCROLL_TEXT_Y = kf.vars["kf_height"] - ROW_HEIGHT - 40 + ROW_HEIGHT - 24           -- scroll text y position
    SCROLL_TEXT_NUMBER_OF_CHARS = 100                                               -- number of chars to display
    SCROLL_TEXT_CHAR_WIDTH = 8                                                      -- widht of a character in the scroll text

    -- Enter Name
    -- -----------------------------------------------------------------------------
    ENTER_NAME_ROWS = 5                                                             -- chars rows
    ENTER_NAME_COLUMNS = 12                                                         -- chars columns  
    CHARS_COLUMN_WIDTH = 16                                                         -- width of a chars column
    CHARS_X_OFFSET = 8                                                              -- offset to center the columns
    SPACE_X_OFFSET = 16                                                             -- space x offset
    CLEAR_X_OFFSET = 64                                                             -- clear x offset
    BACK_X_OFFSET = 112                                                             -- back x offset
    OK_X_OFFSET = 152                                                               -- ok x offset 
   
    -- HIGHSCORE
    -- -----------------------------------------------------------------------------
    FILLER_FACTOR = 1.7                                                             -- Multiplier for the dots
    FILLER_X_LENGTH = 6                                                             -- length of a dot   
    HIGHSCORE_SCORE_X = 264                                                         -- highscore table score x position
    HIGHSCORE_NAME_X = 292                                                          -- highscore table name x position
    HIGHSCORE_FILLER_X = 502                                                        -- highscore table filler x position
    HIGHSCORE_POINTS_X = 512                                                        -- highscore table points x position
    HIGHSCORE_VIEWER_Y_OFFSET = 304                                                 -- y start position of the table
    HIGHSCORE_Y_OFFSET = 4                                                          -- y offset
    HIGHSCORE_VIEWER_GAME_TYPE_Y = 300                                              -- y position game type in the highscore viewer                                                    -- y offset
    HIGHSCORE_POINT_STRING_LENGTH = 3                                               -- the string length of the points 
    HIGHSCORE_TIME_STRING_LENGTH = 3                                                -- the string length of the time
    HIGHSCORE_PLACE_STRING_LENGTH = 2                                               -- the string length of the places
    HIGHSCORE_TABLE_ROW = 12                                                        -- highscore table row
    HIGHSCORE_NAME = 1                                                              -- enum for default values 1 = name
    HIGHSCORE_POINTS = 2                                                            -- enum for default values 2 = points
    HIGHSCORE_SCORES_TO_SHOW = 10                                                   -- number of entries in highscorelist to show
    HIGHSCORE_TABLE_GAME_TYPE_ROW = 1                                               -- row of the game type in the highscore table
    HIGHSCORE_TABLE_EXTRA_LINES = 3                                                 -- row of the game type + 2 empty rows in the highscore table

    -- Game start, Game ends
    -- -----------------------------------------------------------------------------
    OVERLAY_TIMER_START = 150
    OVERLAY_DURATION = 50
    OVERLAY_IMAGE_Y = 185
    OVERLAY_NUMBER_Y = 305

    -- Game
    -- -----------------------------------------------------------------------------
    GAME_LIFES_X = 579                                                              -- lifes x position
    GAME_SCORE_X = 164                                                              -- score x position in single player games
    GAME_TIME_X = 568                                                               -- time x position in single player games
    GAME_SCORE_X_MULTI = 5                                                          -- score x position in multi player games
    GAME_TIME_X_MULTI = 700                                                         -- time x position in multi player games

    -- dying animation
    DYING_DURATION = 32                                                             -- animation duration
    DYING_STEP = 0.2                                                                -- animation step = speed
    PLAYER_DYING_ANIMATION_STEP = 8                                                 -- number of image that display a dying player

    -- movements
    -- -----------------------------------------------------------------------------
    DO_NOT_MOVE = 0                                                                 -- do not move (speed = zero)

    -- other
    KF_CENTER = 2                                                                   -- used in terms like WIDTH - Lenght / 2. Meaning is to centered a y position by deviding the space by 2

    -- Model
    -- ============================================================================================================================
    -- Player
    -- -----------------------------------------------------------------------------
    PLAYER_WIDTH = 32                                                               -- player width
    PLAYER_QUAD_HEIGHT = 32                                                         -- player quad height
    PLAYER_HEIGHT = 28                                                              -- player height
    PLAYER_X_START = 12                                                             -- x start position
    PLAYER_BORDER_LEFT = 0                                                          -- left border
    PLAYER_BORDER_BOTTOM = 200                                                      -- bottom border
    PLAYER_MOVE_DELAY = 2                                                           -- forward speed
    PLAYER_DELAY_STEP = 1                                                           -- moving delay speed
    PLAYER_START_ANIMATION = 1                                                      -- first animation step
    PLAYER_NO_LIFES_LEFT = 0                                                        -- number of lifes that defines a game over
    PLAYER_START_SCORE = 0                                                          -- start score
    PLAYER_DEEP_DEFAULT = 5                                                         -- the default value of the deepness (on groupd) 
    PLAYER_DEEP_RESISTANCE = 4                                                      -- restistance of water deepness 

    -- Creature
    -- -----------------------------------------------------------------------------
    CREATURE_HEIGHT = 28                                                            -- creature height
    CREATURE1_WIDTH = 32                                                            -- stop creature width
    CREATURE_SPEED = 1                                                              -- minimum speed
    CREATURE_DIRECTION_LEFT = -1                                                    -- number representation of left direction
    CREATURE_DIRECTION_RIGHT = 1                                                    -- number representation of right direction
    CREATURE_X_START = 30                                                           -- minimum x start position

    -- Game
    -- -----------------------------------------------------------------------------
    -- Borders
    GAME_BORDERS = {
        up=ROW_UPPER_BORDER,                                                        -- up = starts after the score and time display
        down=PLAYER_BORDER_BOTTOM,                                                  -- down = screen height - 1 Row
        left=PLAYER_BORDER_LEFT,                                                    -- zero
        right=kf.vars["kf_width"] - PLAYER_WIDTH                                    -- screen width - object width 
    }
    
    -- Texts
    SCROLL_TEXT = "                                                                                                         add greetings here                                                                                                         "
    STORY_TEXTS = {                                                                 -- set texts
       "STORY",
       "ADD MORE TEXT HERE"
    }
    CREDIT_TEXTS = {
        "CREDITS",
        "ADD MORE TEXT HERE"
    }
    HOW_TO_PLAY_TEXTS = {
        "HOW TO PLAY",
        "ADD MORE TEXT HERE"
    }

    -- MENU
    -- -----------------------------------------------------------------------------
    MINIMUM_TIME = 30                                                               -- minimum start time
    PW_LOGO_HEIGHT = 40                                                             -- Height of the PW Logo

    -- Controller
    -- ============================================================================================================================

    -- Game
    -- -----------------------------------------------------------------------------
    ZERO_TIME_GAME_OVER = 0                                                         -- when this time is reached, the game is over
    ZERO_NO_PLAYER_ALIVE = 0                                                        -- no player is alive anymore, end the game
end

cConstants:init()