-- Initialize custom code
-- ****************************************************************************************************************************
-- Initialize custom constants 
require ("src/custom/cConstants")                                               -- initialize custom constants

-- Import custom code
-- -----------------------------------------------------------------------------
cMaps = require ("assets/data/cMaps")                                           -- custom maps
-- Initialize custom data
cMaps:init()                                                                    -- initialize custom maps

-- Import custom code specific stuff
-- ============================================================================================================================
-- classes
require ("src/custom/game/cPlayer")                                             -- the player
require ("src/custom/game/cCreature")                                           -- the creature
require ("src/custom/game/cStartpoints")                                        -- the starting points
