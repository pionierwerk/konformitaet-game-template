------------------------------------------------------------------------------------
-- cCreature.lua
-- represents the enemies in the game
--
-- @classmod Creature
-- @author þorN 
-- @license zlib
-- @copyright þorN  for pionierwerk.eu 2018
------------------------------------------------------------------------------------

--- member variables.
-- @table members
-- @number                  y_start                 when a reset is done, the y position will be set to this position
-- @number                  speed                   the speed. This affects the movement of the creature. 
-- @number                  direction               [-1 = links |1= rechts] 

--- member variables.
-- @table members
Creature = setmetatable({},{__index = KFActor})
Creature.__index = Creature

-------------------------------------------------------------------------------------
--- constructor.
--
-- @number                  creature_number         the creature number
-- @number                  y_start                 when a reset is done, the y position will be set to this position
-- @number                  direction               [-1 = links |1= rechts] 
-- @bool                    isActive                activity flag
function Creature:new(creature_number,y_start,direction,isActive)
    -- super
    local self = setmetatable(KFActor:new(isActive), Creature)

    -- set input values
    -- -----------------------------------------------------------------------------
    self.y_start = y_start                                                          -- set the y_start value 
    self.direction = direction                                                      -- set the direction

    -- set class member variables
    self.speed = CREATURE_SPEED                                                     -- set the speed to a random value 

    local _sprite = KFSprite:new("creature","creature".. creature_number,assets.img.enemy,2,true)
    _sprite:setQuads(1,0,0,CREATURE1_WIDTH,CREATURE_HEIGHT)
    _sprite:setImage(1)
    _sprite:setCollionsTargets({
        {x=0,y=4,width=CREATURE1_WIDTH-1,height=23}
    })
    _sprite:setBorders(GAME_BORDERS)
    self:addSprite(_sprite)
    kf:addSprite(_sprite)

    self.width,self.height = self.sprites["creature"].image:getTextureDimensions()  -- set widht,height that depends on the choosen creature type image
    self:reset()

    return self
end

------------------------------------------------------------------------------------
-- moves the creature in the love2d update phase
function Creature:move()
    self.sprites["creature"].x = self.sprites["creature"].x + self.sprites["creature"].moving_vector["x"] -- change the x position based on the running vector
    
    if self.sprites["creature"].x < self.sprites["creature"].borders["left"] - self.width then                              -- test if the x position passed the left border
        self:reset()                                                                -- yes: reset the creature position
    elseif self.sprites["creature"].x > self.sprites["creature"].borders["right"] + self.width then -- test if the x position passed the right border
        self:reset()                                                                -- yes: reset the creature position
    end
end

------------------------------------------------------------------------------------
-- reset the x,y position of the creature
function Creature:reset()
    self.sprites["creature"].y = self.y_start                                       -- set the y position to the y_start value

    if self.direction == CREATURE_DIRECTION_LEFT then                               -- test in wich direction the creature moves
        self.sprites["creature"].moving_vector = {x = self.speed * CREATURE_DIRECTION_LEFT,y = DO_NOT_MOVE} -- left: set running vector with a movement to the left
        self.sprites["creature"].x = self.sprites["creature"].borders.right         -- left: set the x position to the right border value
    else
        self.sprites["creature"].moving_vector = {x = self.speed * CREATURE_DIRECTION_RIGHT,y = DO_NOT_MOVE} -- right: set the running vector with a movement to the right
        self.sprites["creature"].x = self.sprites["creature"].borders.left - self.width -- right: set the x position to left bordre - the width of the creature
    end
end