------------------------------------------------------------------------------------
-- cStartpoints.lua
-- represents the startpoints of the players in the game
--
-- @classmod Startpoints
-- @author þorN 
-- @license zlib
-- @copyright þorN  for pionierwerk.eu 2018
------------------------------------------------------------------------------------

--- member variables.
-- @table members
-- @tab                     areFree                 either free(true) or used(false)
-- @tab                     order                   the order to test if a start point is free
Startpoints = {
    areFree = {},  
    order = {}
}

Startpoints.__index = Startpoints

setmetatable(Startpoints, {
    __call = function (cls, ...)
        return cls.new(...)
    end
})

------------------------------------------------------------------------------------
-- constructor
--
-- @number                  y_start                 when a reset is done, the y position will be set to this position
-- @number                  direction               [-1 = links |1= rechts] 
-- @tab                     borders                 a rectangle that acts as border for the creature
function Startpoints:new(y_start,direction,borders)
    local self = setmetatable({}, Startpoints)

    self.areFree = {true,true,true,true,true,true,true,true,true,true,true,true}  
    self.order = {8,9,7,10,6,11,5,12,4,13,3,14,2,15,1,16}

    return self
end

------------------------------------------------------------------------------------
-- get a random starting point. This function is called at the beginning of a game
-- thats why the random number is in a range of starting point 5-12 
--
-- @return              the free starting position
function Startpoints:getRandomStartPoint()
    local _not_found = true
    local _rnd_number = 0
    
    while _not_found do
        _rnd_number = math.random(5,12)
        if self.areFree[_rnd_number] then
            _not_found = false
        end
    end

    self.areFree[_rnd_number] = false
    return _rnd_number
end

------------------------------------------------------------------------------------
-- get the next free place. This could be in the whole range of starting point (1-12)
function Startpoints:getNextFreeStartPoint()  
    for _number =1,16 do
        if self.areFree[self.order[_number]] then
            self.areFree[self.order[_number]] = false
            return self.order[_number]
        end
    end
end

------------------------------------------------------------------------------------
-- clear the given start point so that it is available for the next players
function Startpoints:clearStartPoint(start_point)
    if start_point > 0 then
        self.areFree[start_point] = true
    end
end