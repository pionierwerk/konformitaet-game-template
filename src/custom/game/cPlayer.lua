------------------------------------------------------------------------------------
-- cPlayer.lua
-- represents the players in the game
--
-- @classmod Player
-- @author þorN 
-- @license zlib
-- @copyright þorN  for pionierwerk.eu 2018
------------------------------------------------------------------------------------

--- member variables.
-- @table members
-- @bool                    isMoving                a flag that indicates, that the playe is moving
-- @number                  animation_step          the step counter for the moving animation
-- @number                  score                   the score of the player
-- @number                  wait_moving             a delay counter to syncronize the moving
-- @number                  moving_counter_start     the start value of the moving counter
-- @bool                    isDying                 a flag to state if the player is dying 
-- @number                  dying_counter           the step counter for the dying animation
-- @tab                     borders                 a rectangle that acts as border for the player
-- @bool                    isWaitingAfterReset     flag to let the player stops after it was reset to the top. This prevents from imediately loosing the next life.
-- @number                  start_point             the actual start point
-- @number                  player_number           the number of the player give during the creation

--- member variables.
-- @table members
Player = setmetatable({},{__index = KFActor})
Player.__index = Player

-------------------------------------------------------------------------------------
--- constructor.
-- @number                  amount                  amount of animation steps
-- @umber                   qx                      quad x start position in image
-- @umber                   qy                      quad y start position in image
-- @bool                    isActive                activity flag
function Player:new(amount,qx,qy,isActive)
    -- super
    local self = setmetatable(KFActor:new(isActive), Player)
    -- set class member variables
    self.player_number = #kf.vars["kf_players"] + 1                                 -- set player number
    self.start_point = 0
    self.isMoving = false
    self.animation_step = 1
    self.score = PLAYER_START_SCORE
    self.wait_moving = 0
    self.isDying= false
    self.dying_counter = 1
    self.isWaitingAfterReset = false

    local _sprite = KFSprite:new("player","player" .. self.player_number, assets.img.player_small,1,true)
    _sprite:setQuads(amount,qx,qy,PLAYER_WIDTH,PLAYER_HEIGHT)
    _sprite.image = _sprite.quads[PLAYER_START_ANIMATION]                           -- set the image to waiting
    _sprite:setCollionsTargets({
        { x = 11, y = 4, width = 15, height = 23 }
    })
    _sprite:setBorders(GAME_BORDERS)
    self:addSprite(_sprite)
    kf:addSprite(_sprite)

    self:reset(false)

    return self
end

------------------------------------------------------------------------------------
--- Starts the moving animation of the player.
-- @number                  y                       the y axis movement for the moving_vector
function Player:start_move(y)
    self.sprites["player"].moving_vector = {                                        -- set the initial moving_vector (NO x move)
        x=DO_NOT_MOVE,
        y=y
    } 

    self.wait_moving = PLAYER_DEEP_RESISTANCE                                       -- NO Delay  

    self.isMoving = true                                                            -- start moving
end

------------------------------------------------------------------------------------
--- Start the dying animation.
-- @sound                   sound                   the sound to play
function Player:start_dying(sound)
    self.isMoving = false                                                           -- stop moving animation
    self.isDying = true                                                             -- start dying animation
    self.dying_counter = DYING_DURATION                                             -- set animation duration

    kf.vars["snds_die"][sound]:play()                                               -- play dying sound
end

------------------------------------------------------------------------------------
--- Process the dead of the player.
function Player:die()
    self.isDying = false                                                            -- stop the dying animation
    self.lifes = self.lifes - 1                                                     -- decrease the lifes

    if self.lifes == PLAYER_NO_LIFES_LEFT then                                      -- test if this was the last life
        self.isActive = false                                                       -- set status that there are no lifes left
    end

    self:reset(true)                                                                -- no: reset player koordinates and image
end

------------------------------------------------------------------------------------
--- Animate the dying of a player.
function Player:animated_dying()
    if self.dying_counter > 0 then                                                  -- test if duration is over
        self.dying_counter = self.dying_counter - DYING_STEP                        -- no: decrease duration
        self.sprites["player"].image = love.graphics.newQuad(PLAYER_WIDTH * PLAYER_DYING_ANIMATION_STEP, 0 + ( (self.player_number-1) * PLAYER_QUAD_HEIGHT ), PLAYER_WIDTH, PLAYER_HEIGHT - (PLAYER_QUAD_HEIGHT - self.dying_counter), self.sprites["player"].sheet:getDimensions())
        self.sprites["player"].y = self.sprites["player"].y + DYING_STEP
    else
        self:die()                                                                  -- yes: die
    end
end

------------------------------------------------------------------------------------
--- Animate the movement of a player.
function Player:move()
    self.wait_moving = self.wait_moving - PLAYER_DELAY_STEP                         -- decrease delay

    if self.wait_moving == kf.DECREASING_COUNTER_TO_ZERO_REACHED then               -- test if delay is over
        self.animation_step = self.animation_step + 1                               -- yes: increase animation step

        if self.animation_step < #self.sprites["player"].quads then                 -- test if animation is finished
            self.sprites["player"].image = self.sprites["player"].quads[self.animation_step]  -- no: set new image
            if      self.sprites["player"].x + self.sprites["player"].moving_vector.x >= self.sprites["player"].borders["left"] -- test if movement reaches borders
                and self.sprites["player"].x + self.sprites["player"].moving_vector.x <= self.sprites["player"].borders["right"] 
                and self.sprites["player"].y + self.sprites["player"].moving_vector.y >= self.sprites["player"].borders["up"] 
                and self.sprites["player"].y + self.sprites["player"].moving_vector.y <= self.sprites["player"].borders["down"] then
                self.sprites["player"].x = self.sprites["player"].x + self.sprites["player"].moving_vector.x -- no: move player based on the moving_vector
                self.sprites["player"].y = self.sprites["player"].y + self.sprites["player"].moving_vector.y 
            end
        else
            self:end_move()                                                         -- yes: finish move
        end

        self.wait_moving = PLAYER_MOVE_DELAY                                        -- reset movement delay to start value
    end
end

------------------------------------------------------------------------------------
--- Finish player movement animation.
function Player:end_move()
    self.isMoving = false                                                           -- stop moving animation
    self.animation_step = 1                                                         -- reset animation step 
    self.sprites["player"].image = self.sprites["player"].quads[PLAYER_START_ANIMATION] -- set image to waiting
end

------------------------------------------------------------------------------------
--- Reset the player after a life is lost or a game starts.
--
-- @bool                    isFindingFreeFlace      either false (random place) or true (find next free place)
function Player:reset(isFindingFreePlace)
    self.sprites["player"].image = self.sprites["player"].quads[PLAYER_START_ANIMATION] -- set the image to waiting
    self.sprites["player"].y = ROW_UPPER_BORDER                                     -- set the y coordinate to the start postion 
    self.wait_moving = PLAYER_MOVE_DELAY                                            -- reset wait moving counter to start value
    self.isMoving = false                                                           -- stop moving
    self.isWaitingAfterReset = true                                                 -- set flag to avoid immediatly move

    local _old_start_point = self.start_point                                       -- store actual start point

    if isFindingFreePlace then                                                      -- test start point calculation mode
        self.start_point = kf.vars["start_points"]:getNextFreeStartPoint()          -- free place mode
    else
        self.start_point = kf.vars["start_points"]:getRandomStartPoint()            -- random place mode
    end

    kf.vars["start_points"]:clearStartPoint(_old_start_point)                       -- clear old start point

    self.sprites["player"].x = PLAYER_X_START + (self.start_point * 48)             -- set x position to start point 
end