return {    
    -- custom variables 
    -- ---------------------------------------------------------------------
    -- menu & game
    map = 1,                                                                -- the actual game map
    forward_speed = 2,                                                      -- the forward speed of the player
    backward_speed = 2,                                                     -- the backward speed of the player
    time_start = 0,                                                         -- the start value of the timer 
    lifes = 9,                                                              -- the number of lifes          
    hs_map = 1,                                                             -- the actual game map
    hs_forward_speed = 2,                                                   -- the forward speed of the player
    hs_backward_speed = 2,                                                  -- the backward speed of the player
    hs_time_start = 0,                                                      -- the start value of the timer 
    hs_lifes = 9,                                                           -- the number of lifes          
    -- game
    snds_win = {                                                            -- sounds for reaching the shore
        assets.sounds.win_aufgehts,
        assets.sounds.win_gotscha,
        assets.sounds.win_gutso,
        assets.sounds.win_jawohl,
        assets.sounds.win_juhhuh,
        assets.sounds.win_sieg,
        assets.sounds.win_tschakka,
        assets.sounds.win_vorwaertz
    },
    snds_die = {                                                            -- sounds when a player dies
        assets.sounds.die_daswarnichts,assets.sounds.die_ichwusstedasklapptnicht,assets.sounds.die_nein,assets.sounds.die_ohnee,assets.sounds.die_programmfehler,assets.sounds.die_sokalt,assets.sounds.die_tot,assets.sounds.die_verdammtnochmal
    },
    game_font = love.graphics.newImageFont("assets/fonts/Imagefont.png",    -- game font
        " abcdefghijklmnopqrstuvwxyz" ..
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ0" ..
        "123456789.,!?-+/():;%&`'*#=[]\""
    ),
    startpoints={},                                                         -- starting points
    -- scroller
    scroll_text_x = 0,
    scroll_text_offset = 0
}