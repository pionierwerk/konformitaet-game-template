# KonFormität game template
this is a template to start with using [KonFormität](https://gitlab.com/pionierwerk/konformitaet).

Clone the tempalate and you will have a fully working game that offers:
- Single player game
- Multi player game

- A logo screen
- A menu screen
- A game screen
- A high score name enter screen
- A high score table screen

- Game parameters changeable in the menu
- Credits screen in the menu
- How to play screen in the menu
- High score table viewer screen in the menu
- Toggle between fullscreen and windowed mode in the menu
- Quit game in the menu

- High score database with tables that depend on the choosen options

- The complete game is controlable either by keyboard or by joystick

- Winning sounds, Loosing sounds
- Background music

# Usage
This template is used in the [Konformität-installer](https://gitlab.com/pionierwerk/konformitaet-initializer)