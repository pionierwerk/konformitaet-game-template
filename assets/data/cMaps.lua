cMaps = {}

function cMaps:init()
    map_names = {"WEST","NORTH","EAST","SOUTH"}                                      -- name of the mapes

    -- maps CREATURE directions
    -- -----------------------------------------------------------------------------
    -- Either CREATURE_DIRECTION_RIGHT or CREATURE_DIRECTION_LEFT
    maps_creature_directions = {
        {
            CREATURE_DIRECTION_RIGHT,
            CREATURE_DIRECTION_RIGHT,
            CREATURE_DIRECTION_RIGHT,
            CREATURE_DIRECTION_LEFT,
        },
        {
            CREATURE_DIRECTION_RIGHT,
            CREATURE_DIRECTION_RIGHT,
            CREATURE_DIRECTION_RIGHT,
            CREATURE_DIRECTION_RIGHT,
        },
        {
            CREATURE_DIRECTION_RIGHT,
            CREATURE_DIRECTION_RIGHT,
            CREATURE_DIRECTION_LEFT,
            CREATURE_DIRECTION_RIGHT,
        },
        {
            CREATURE_DIRECTION_RIGHT,
            CREATURE_DIRECTION_RIGHT,
            CREATURE_DIRECTION_LEFT,
            CREATURE_DIRECTION_LEFT,
        }
    }

end

return cMaps