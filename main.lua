function love.load()
    -- Import libraries
    -- ============================================================================================================================
    -- lua libaries
    -- -----------------------------------------------------------------------------
    math = require("math")
    os = require("os")

    -- external libraries
    -- -----------------------------------------------------------------------------
    json = require ("lib/dkjson/dkjson")                                            -- json library
    assets = require("lib/cargo/cargo").init('assets')                              -- load asset with cargo
    binser = require("lib/binser/binser")                                           -- import serializer
    bit32 = require 'lib/lua-bit-numberlua/lmod/bit/numberlua'.bit32

    -- KonFormitaet libaries
    -- -----------------------------------------------------------------------------
    require ("lib/konformitaet/konformitaet")                                           -- the kernel

    -- Import KonFormitaet customizsations
    -- ============================================================================================================================
    require ("src/customizations/kfcMenu")                                          -- menu customizations
    require ("src/customizations/kfcTransitions")                                   -- (customized) transitions
    require ("src/customizations/kfcViews")                                         -- the views
    require ("src/customizations/kfcGame")                                          -- the game state and transitions

    -- Initializations
    -- ============================================================================================================================
    -- Initialize KonFormitaet by importing configurations
    -- ****************************************************************************************************************************
    kf=KFKernel:new(                                                                -- create a kernel
        json.decode (kfUtils:read_file("config/kfConfig.json"), 1, nil),            -- read in the menu configuration written in KonFormitaet menu DSL
        require ("config/cVars")                                                    -- read in the custom vars
    )

    -- Initialize KonFormitaet customizations
    -- ****************************************************************************************************************************
    -- Import KFML
    -- -----------------------------------------------------------------------------
    kf:processKFML(json.decode (kfUtils:read_file("config/kfml.json"), 1, nil))  -- read in the menu configuration written in KonFormitaet menu DSL
  
    -- Custom initialization
    -- ****************************************************************************************************************************
    require ("src/custom/cInit")
    
    -- start KonFormitaet
    -- ============================================================================================================================
    kf:start()                                                                      -- start with the given first state
end

--- Love2d update function.
-- ================================================================================================================================
function love.update()
    kf:update()                                                                     -- call the wrapper function that will call the actual update function
end

--- Love2d draw function.
-- ================================================================================================================================
function love.draw()
    kf:draw()                                                                       -- call the wrapper function that will call the actual draw function
end
